/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// GitLabSpec defines the desired state of GitLab
type GitLabSpec struct {
	Version     string    `json:"version"`
	Templates   Templates `json:"templates"`
	HelmRelease string    `json:"helmRelease"`
}

type Templates struct {
	MigrationsTemplate    TemplateSpec      `json:"migrationsTemplate"`
	SharedSecretsTemplate SharedSecretsSpec `json:"sharedSecretsTemplate"`
}

type TemplateSpec struct {
	ConfigMapName string `json:"configMapName"`
	ConfigMapKey  string `json:"configMapKey"`
}

type SharedSecretsSpec struct {
	ServiceAccountKey string `json:"serviceAccountKey"`
	RoleKey           string `json:"roleKey"`
	RoleBindingKey    string `json:"roleBindingKey"`
	TemplateSpec
}

// GitLabStatus defines the observed state of GitLab
type GitLabStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// GitLab is the Schema for the gitlabs API
// +k8s:openapi-gen=true
type GitLab struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GitLabSpec   `json:"spec,omitempty"`
	Status GitLabStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// GitLabList contains a list of GitLab resources
type GitLabList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitLab `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GitLab{}, &GitLabList{})
}
