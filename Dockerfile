# Build the manager binary
FROM golang:1.10.3 as builder

# Copy in the go src
WORKDIR /go/src/gitlab.com/charts/components/gitlab-operator
COPY . .

# Build
RUN make docker-manager

# Copy the controller-manager into a thin image
FROM ubuntu:latest
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/charts/components/gitlab-operator/bin/manager .
ENTRYPOINT ["./manager"]
